<?php
/**
 * DokuWiki Plugin authetwin (Action Component)
 */

// must be run within Dokuwiki
if(!defined('DOKU_INC')) die();

class action_plugin_authetwin extends DokuWiki_Action_Plugin {
    /**
     * Registers a callback function for a given event
     *
     * @param Doku_Event_Handler $controller DokuWiki's event controller object
     * @return void
     */
    public function register(Doku_Event_Handler $controller) {
        global $conf;
        if($conf['authtype'] != 'authetwin') return;

        $conf['profileconfirm'] = false; // password confirmation doesn't work with oauth only users

        $controller->register_hook('DOKUWIKI_STARTED', 'BEFORE', $this, 'handle_start');
    }

    /**
     * Start an oAuth login or restore  environment after successful login
     *
     * @param Doku_Event $event  event object by reference
     * @param mixed      $param  [the parameters passed as fifth argument to register_hook() when this
     *                           handler was registered]
     * @return void
     */
    public function handle_start(Doku_Event &$event, $param) {
        global $ACT;
        if ($ACT == 'login') {
            $this->startOauthLogin();
        } else {
            return;
        }
    }

    private function startOauthLogin() {
        global $ID;
        /** @var helper_plugin_authetwin $hlp */
        $hlp         = plugin_load('helper', 'authetwin');
        $client     = $hlp->loadOauthClient();
        $url = $client->getAuthorizationUri("base", $ID);
        send_redirect($url);
    }
}
