<?php

namespace Etwin\OauthClient;

final class DokuEtwinClient {
    /** @var string */
    private $etwinUri;

    /** @var \dokuwiki\HTTP\DokuHTTPClient  */
    private $client;

    /**
     * DokuEtwinClient constructor.
     *
     * @param string $etwinUri
     */
    final public function __construct(
        $etwinUri,
    ) {
        $this->etwinUri = $etwinUri;
        $this->client = new \dokuwiki\HTTP\DokuHTTPClient();
    }

    /**
     * @param string $atKey Access token key.
     * @return mixed Return current auth.
     */
    final public function getSelf($atKey) {
        $this->client->headers["Authorization"] = $this->getAuthorizationHeader($atKey);
        $raw = $this->client->get(
            $this->etwinUri . '/api/v1/auth/self'
        );
        return json_decode($raw);
    }

    /**
     * @return string
     */
    final private function getAuthorizationHeader($atKey) {
        return "Bearer " . $atKey;
    }
}
