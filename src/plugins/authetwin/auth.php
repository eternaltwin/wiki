<?php
use dokuwiki\Utf8\Sort;

/**
 * Eternaltwin authentication
 */
class auth_plugin_authetwin extends DokuWiki_Auth_Plugin
{
    /** @var array user cache */
    protected $users = null;

    /**
     * Constructor.
     *
     * Carry out sanity checks to ensure the object is
     * able to operate. Set capabilities in $this->cando
     * array here
     *
     * For future compatibility, sub classes should always include a call
     * to parent::__constructor() in their constructors!
     *
     * Set $this->success to false if checks fail
     *
     * @author  Christopher Smith <chris@jalakai.co.uk>
     */
    public function __construct()
    {
        parent::__construct();
        $this->cando['external'] = true;
    }

    /**
     * Log off the current user [ OPTIONAL ]
     *
     * Is run in addition to the ususal logoff method. Should
     * only be needed when trustExternal is implemented.
     *
     * @see     auth_logoff()
     * @author  Andreas Gohr <andi@splitbrain.org>
     */
    public function logOff()
    {
        parent::logOff();
        if(isset($_SESSION[DOKU_COOKIE]['auth'])) {
            unset($_SESSION[DOKU_COOKIE]['auth']);
        }
    }

    /**
     * Do all authentication [ OPTIONAL ]
     *
     * Set $this->cando['external'] = true when implemented
     *
     * If this function is implemented it will be used to
     * authenticate a user - all other DokuWiki internals
     * will not be used for authenticating (except this
     * function returns null, in which case, DokuWiki will
     * still run auth_login as a fallback, which may call
     * checkPass()). If this function is not returning null,
     * implementing checkPass() is not needed here anymore.
     *
     * The function can be used to authenticate against third
     * party cookies or Apache auth mechanisms and replaces
     * the auth_login() function
     *
     * The function will be called with or without a set
     * username. If the Username is given it was called
     * from the login form and the given credentials might
     * need to be checked. If no username was given it
     * the function needs to check if the user is logged in
     * by other means (cookie, environment).
     *
     * The function needs to set some globals needed by
     * DokuWiki like auth_login() does.
     *
     * @see     auth_login()
     * @author  Andreas Gohr <andi@splitbrain.org>
     *
     * @param   string $user Username
     * @param   string $pass Cleartext Password
     * @param   bool $sticky Cookie should not expire
     * @return  bool         true on successful auth,
     *                       null on unknown result (fallback to checkPass)
     */
    public function trustExternal($user, $pass, $sticky = false)
    {
        global $ACT, $INPUT, $USERINFO;
        if ($ACT == "logout") {
            unset($_SESSION[DOKU_COOKIE]['auth']);
            $USERINFO = null;
        }
        if (!isset($_SESSION[DOKU_COOKIE]['auth'])) {
            $_SESSION[DOKU_COOKIE]['auth'] = array();
        }
        $session = $_SESSION[DOKU_COOKIE]['auth'];

        if (array_key_exists('id', $_REQUEST) && $_REQUEST['id'] == "oauth/callback") {
            /** @var helper_plugin_authetwin $hlp */
            $hlp = plugin_load('helper', 'authetwin');
            $oauthClient = $hlp->loadOauthClient();

            if (!$INPUT->get->has('code') || !$INPUT->get->has('state')) {
                return false;
            }
            $code = $INPUT->str("code");
            $state = $INPUT->str("state");
            $at = $oauthClient->getAccessTokenSync($code);

            $etwinClient = $hlp->loadEtwinClient();

            $self = $etwinClient->getSelf($at->access_token);

            if ($self != null) {
                $id = $self->user->id;
                $name = $self->user->display_name->current->value;

                $info = array();
                $info['id'] = $id;
                $info['name'] = htmlspecialchars($name);
                $info['mail'] = $id . "@users";
                $info['grps'] = ['user'];

                $USERINFO['name'] = $info['name'];
                $USERINFO['mail'] = $info['mail'];
                $USERINFO['grps'] = $info['grps'];
                $_SERVER['REMOTE_USER'] = $id;
                $_SESSION[DOKU_COOKIE]['auth']['user'] = $id;
                $_SESSION[DOKU_COOKIE]['auth']['pass'] = $pass;
                $_SESSION[DOKU_COOKIE]['auth']['info'] = $USERINFO;

                if ($this->users === null) {
                    $this->loadUserData();
                }
                if (!isset($this->users[$id])) {
                    $this->users[$id] = $info;
                    $this->writeUserFile($this->users);
                } else {
                    $old = $this->users[$id];
                    if ($old['name'] != $info['name']) {
                        $this->users[$id]['name'] = $info['name'];
                    }
                    $this->writeUserFile($this->users);
                }

                send_redirect(wl($state));
                return true;
            }
            send_redirect(wl("start"));
        } else if (isset($session['info']) && isset($session['user'])) {
            $USERINFO = $session['info'];
            $_SERVER['REMOTE_USER'] = $session['user'];
            return true;
        }
        return false;
    }

    /**
     * Check user+password
     *
     * Checks if the given user exists and the given
     * plaintext password is correct
     *
     * @author  Andreas Gohr <andi@splitbrain.org>
     * @param string $user
     * @param string $pass
     * @return  bool
     */
    public function checkPass($user, $pass)
    {
        return false;
    }

    /**
     * Return user info [ MUST BE OVERRIDDEN ]
     *
     * Returns info about the given user needs to contain
     * at least these fields:
     *
     * name string  full name of the user
     * mail string  email address of the user
     * grps array   list of groups the user is in
     *
     * @author  Andreas Gohr <andi@splitbrain.org>
     * @param   string $user the user name
     * @param   bool $requireGroups whether or not the returned data must include groups
     * @return  false|array containing user data or false
     */
    public function getUserData($user, $requireGroups = true)
    {
        if ($this->users === null) {
            $this->loadUserData();
        }
        return isset($this->users[$user]) ? $this->users[$user] : false;
    }

    /**
     * Load all user data
     *
     * loads the user file into a datastructure
     *
     * @author  Andreas Gohr <andi@splitbrain.org>
     */
    protected function loadUserData()
    {
        $this->users = $this->readUserFile();
    }

    /**
     * Read user data from given file
     *
     * ignores non existing files
     *
     * @return array
     */
    private function readUserFile()
    {
        $file = $this->getUserFile();
        $users = array();
        if (!file_exists($file)) return $users;

        $json = file_get_contents($file);
        return json_decode($json, true);
    }

    private function writeUserFile($users)
    {
        $json = json_encode($users, JSON_PRETTY_PRINT);
        file_put_contents($this->getUserFile(), $json);
    }

    private function getUserFile()
    {
        return DOKU_CONF . 'users.etwin.json';
    }
}
