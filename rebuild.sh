#!/bin/bash
set -eux
sudo rm -rf build/
cargo xtask build
sudo ./refresh-permissions.sh
