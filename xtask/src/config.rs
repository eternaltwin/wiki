use serde::Deserialize;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use thiserror::Error;

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct RawConfig {
  pub external_uri: String,
  pub etwin_uri: String,
  pub etwin_client_id: String,
  pub etwin_client_secret: String,
  pub smtp_key: String,
}

#[derive(Debug, Error)]
pub enum FindConfigFileError {
  #[error("No configuration file found from: {:?}", .0)]
  NotFound(PathBuf),
  #[error("Unexpected I/O error when resolving configuration from: {:?}: {:?}", .0, .1)]
  Other(PathBuf, io::Error),
}

#[derive(Debug, Error)]
pub enum FindConfigError {
  #[error("No configuration file found from: {:?}", .0)]
  NotFound(PathBuf),
  #[error("Failed to parse configuration file at: {:?}: {:?}", .0, .1)]
  ParseError(PathBuf, toml::de::Error),
  #[error("Unexpected I/O error when resolving configuration from: {:?}: {:?}", .0, .1)]
  Other(PathBuf, io::Error),
}

fn find_config_file(dir: PathBuf) -> Result<(PathBuf, String), FindConfigFileError> {
  for d in dir.ancestors() {
    let config_path = d.join("wiki.toml");
    match fs::read_to_string(&config_path) {
      Ok(toml) => return Ok((config_path, toml)),
      Err(e) if e.kind() == io::ErrorKind::NotFound => continue,
      Err(e) => return Err(FindConfigFileError::Other(dir, e)),
    }
  }
  Err(FindConfigFileError::NotFound(dir))
}

pub fn parse_config(_file: &Path, config_toml: &str) -> Result<RawConfig, toml::de::Error> {
  let raw: RawConfig = toml::from_str(&config_toml)?;
  Ok(raw)
}

pub fn find_config(dir: PathBuf) -> Result<RawConfig, FindConfigError> {
  match find_config_file(dir) {
    Ok((file, config_toml)) => match parse_config(&file, &config_toml) {
      Ok(config) => Ok(config),
      Err(e) => Err(FindConfigError::ParseError(file, e)),
    },
    Err(FindConfigFileError::NotFound(dir)) => Err(FindConfigError::NotFound(dir)),
    Err(FindConfigFileError::Other(dir, cause)) => Err(FindConfigError::Other(dir, cause)),
  }
}
