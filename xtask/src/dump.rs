use std::error::Error;
use std::fs;
use std::path::PathBuf;
use crate::copy_dir_rec;

pub struct Dirs {
  pub build: PathBuf,
  pub snapshot: PathBuf,
}

impl Dirs {
  pub fn new() -> Self {
    let root = std::env::current_dir().unwrap();
    let build: PathBuf = root.join("build");
    let snapshot: PathBuf = root.join("snapshot");
    Self {
      build,
      snapshot,
    }
  }
}

pub async fn dump() -> Result<(), Box<dyn Error>> {
  let dirs = Dirs::new();

  eprintln!("Empty snapshot/");
  if dirs.snapshot.exists() {
    fs::remove_dir_all(&dirs.snapshot).unwrap();
  }
  fs::create_dir(&dirs.snapshot).unwrap();

  eprintln!("Backup pages");
  copy_dir_rec(&dirs.build.join("data/pages"), &dirs.snapshot.join("pages"));
  eprintln!("Backup meta");
  copy_dir_rec(&dirs.build.join("data/meta"), &dirs.snapshot.join("meta"));
  eprintln!("Backup media");
  copy_dir_rec(&dirs.build.join("data/media"), &dirs.snapshot.join("media"));
  eprintln!("Backup media_meta");
  copy_dir_rec(&dirs.build.join("data/media_meta"), &dirs.snapshot.join("media_meta"));
  eprintln!("Backup attic");
  copy_dir_rec(&dirs.build.join("data/attic"), &dirs.snapshot.join("attic"));
  eprintln!("Backup media_attic");
  copy_dir_rec(&dirs.build.join("data/media_attic"), &dirs.snapshot.join("media_attic"));

  Ok(())
}
